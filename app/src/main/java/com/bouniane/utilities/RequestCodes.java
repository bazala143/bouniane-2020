package com.bouniane.utilities;

public class RequestCodes {
    public static final int REQUEST_SPLASH_ACTIVITY = 301;
    public static final int REQUEST_MAIN_ACTIVITY = 302;
    public static final int REQUEST_LOGIN_ACTIVITY = 303;
    public static final int REQUEST_VERIFY_OTP_ACTIVITY = 305;
    public static final int REQUEST_CHOOSE_MAP_LOCATION_ACTIVITY = 306;
    public static final int REQUEST_GOOGLE_LOGIN = 111;

    public static int PHONE_REQUEST_CODE = 191;
}
