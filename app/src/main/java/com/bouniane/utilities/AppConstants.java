package com.bouniane.utilities;

public class AppConstants {
    /*Stings*/
    public static final String PREF_NAME = "bounianae_prefs";
    public static final String TITLE = "title";
    public static final String MESSAGE = "message";
    public static final String DEVICE_TYPE = "1";


    /*Languages*/
    public static final String LANGUAGE_ARABIC = "ar";
    public static final String LANGUAGE_ENGLISH = "en";
    public static final String LANGUAGE_URDU = "en";


    /*Others*/
    public static final String EXTRA_MODEL = "EXTRA_MODEL";
    public static final String MOBILE_NUMBER = "MOBILE_NUMBER";
    public static final String NAME = "NAME";
    public static final String EMAIL = "EMAIL";
    public static final String PASSWORD = "PASSWORD";
    public static final String FROM_SCREEN = "FROM_SCREEN";
    public static final String OTP = "OTP";
    public static final String STATUS_OK = "OK";
    public static final String ADDRESS_TYPE = "ADDRESS_TYPE";
    public static final String FULL_ADDRESS = "FULL_ADDRESS";
    public static final String LATITUDE = "LATITUDE";
    public static final String LONGITUDE = "LONGITUDE";
    public static final String FROM = "FROM";
    public static final String TO = "TO";

    public static final String PUSH_NOTIFICATION = "PUSH_NOTIFICATION";

    /* socials*/
    public static final String TYPE_FACEBOOK = "1";
    public static final String TYPE_GOOGLE = "2";
    public static final String TYPE_APPLE = "3";
    public static String PHONE_CODE = "+91";
    public static String VERIFIED = "1";
    public static String NOT_VERIFIED = "0";
    public static String SOCIAL = "1";
    public static String NOT_SOCIAL = "0";
}
