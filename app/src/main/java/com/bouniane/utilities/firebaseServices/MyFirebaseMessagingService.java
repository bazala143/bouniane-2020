package com.bouniane.utilities.firebaseServices;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;


import com.bouniane.prefs.AppPreferencesHelper;
import com.bouniane.ui.activities.main.MainActivity;
import com.bouniane.utilities.AppConstants;
import com.bouniane.utilities.CommonUtils;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    AppPreferencesHelper sharedPref;
    Intent chatIntent = null;
    boolean flag = false;
    private NotificationUtils mNotificationUtils;

    @SuppressWarnings("unused")
    public MyFirebaseMessagingService() {
    }

    @Override
    public void onNewToken(String refreshedToken) {
        super.onNewToken(refreshedToken);
        CommonUtils.pLog(TAG, "Refreshed token: " + refreshedToken);
        AppPreferencesHelper sharedPref = new AppPreferencesHelper(getApplicationContext(), AppConstants.PREF_NAME);
        sharedPref.setDeviceToken(refreshedToken);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        mNotificationUtils = new NotificationUtils(getApplicationContext());
        sharedPref = new AppPreferencesHelper(getApplicationContext(), AppConstants.PREF_NAME);
        Log.e("Notification", "Received");


        /*if (sharedPref.getLoginModel() != null) {
            JSONObject jsonObject = null;
            Map<String, String> data = remoteMessage.getData();
            jsonObject = new JSONObject(data);
            CommonUtils.pLog(TAG, "Notification : " + jsonObject.toString());

            if (jsonObject.has("nType")) {
                try {
                    int nType = Integer.parseInt(jsonObject.optString("nType"));
                    String title = jsonObject.optString("title");
                    String message = jsonObject.optString("message");
                    String orderId = jsonObject.optString("id");
                    sendAppNotification(title, message, nType, orderId);
                } catch (Exception e) {
                    e.printStackTrace();
                    CommonUtils.pLog("error", e.getMessage());
                }
            } else {
                getReceiverDetail(jsonObject);
            }

        }*/

    }

    private void sendAppNotification(String title, String message, int nType, String orderId) {
        CommonUtils.pLog("TAG", "App Notification");
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(AppConstants.FROM_SCREEN, AppConstants.PUSH_NOTIFICATION);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        mNotificationUtils.getManager().notify((int) System.currentTimeMillis(),
                mNotificationUtils.getNotificationBuilder(intent, title, message, false));
        @SuppressLint("InvalidWakeLockTag")
        PowerManager.WakeLock wakeLock = ((PowerManager) getSystemService(POWER_SERVICE)).newWakeLock(PowerManager.FULL_WAKE_LOCK
                | PowerManager.ACQUIRE_CAUSES_WAKEUP, "PowerWakeLock");

        wakeLock.acquire(15000);
    }




}
