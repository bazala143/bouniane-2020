/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.bouniane.di.component;


import com.bouniane.di.PerActivity;
import com.bouniane.di.module.ActivityModule;
import com.bouniane.networking.NetworkModule;
import com.bouniane.ui.activities.login.LoginActivity;
import com.bouniane.ui.activities.main.MainActivity;
import com.bouniane.ui.activities.signUpWith.SignUpWithActivity;
import com.bouniane.ui.activities.signup.SignUpActivity;
import com.bouniane.ui.activities.splash.SplashActivity;
import com.bouniane.ui.fragments.employer.EmployerFragment;
import com.bouniane.ui.fragments.freelancer.FreelancerFragment;
import com.bouniane.ui.fragments.freelancerJobList.FreelancerJobsFragment;
import com.bouniane.ui.fragments.main.MainFragment;
import com.bouniane.ui.fragments.otpBottomSheet.OtpDialogFragment;

import dagger.Component;


@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, NetworkModule.class,})
public interface ActivityComponent {

    /*Activities*/
    /*------------------------------------------------------*/

    void inject(SplashActivity activity);

    void inject(LoginActivity activity);

    void inject(MainActivity activity);

    void inject(SignUpActivity activity);

    void inject(SignUpWithActivity activity);


    /*Fragments*/
    /*------------------------------------------------------*/

    void inject(MainFragment fragment);

    void inject(EmployerFragment fragment);

    void inject(FreelancerFragment fragment);

    void inject(FreelancerJobsFragment fragment);

    void inject(OtpDialogFragment fragment);


}
