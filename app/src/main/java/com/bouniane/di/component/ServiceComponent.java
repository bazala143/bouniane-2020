package com.bouniane.di.component;



import com.bouniane.broadcastReceiver.AlarmReceiver;
import com.bouniane.di.PerActivity;
import com.bouniane.networking.NetworkModule;

import dagger.Component;

@PerActivity
@Component(modules = NetworkModule.class)
public interface ServiceComponent {
    void injectAlarmReceiver(AlarmReceiver alarmReceiver);
}
