/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.bouniane.di.module;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bouniane.di.ActivityContext;
import com.bouniane.di.PerActivity;
import com.bouniane.di.PreferenceInfo;
import com.bouniane.prefs.AppPreferencesHelper;
import com.bouniane.prefs.PreferencesHelper;
import com.bouniane.ui.activities.login.LoginMvpPresenter;
import com.bouniane.ui.activities.login.LoginMvpView;
import com.bouniane.ui.activities.login.LoginPresenter;
import com.bouniane.ui.activities.main.MainMvpPresenter;
import com.bouniane.ui.activities.main.MainMvpView;
import com.bouniane.ui.activities.main.MainPresenter;
import com.bouniane.ui.activities.signUpWith.SignUpWithMvpPresenter;
import com.bouniane.ui.activities.signUpWith.SignUpWithMvpView;
import com.bouniane.ui.activities.signUpWith.SignUpWithPresenter;
import com.bouniane.ui.activities.signup.SignUpMvpPresenter;
import com.bouniane.ui.activities.signup.SignUpMvpView;
import com.bouniane.ui.activities.signup.SignUpPresenter;
import com.bouniane.ui.activities.splash.SplashMvpPresenter;
import com.bouniane.ui.activities.splash.SplashMvpView;
import com.bouniane.ui.activities.splash.SplashPresenter;
import com.bouniane.ui.fragments.employer.EmployerFragmentMvpPresenter;
import com.bouniane.ui.fragments.employer.EmployerFragmentMvpView;
import com.bouniane.ui.fragments.employer.EmployerFragmentPresenter;
import com.bouniane.ui.fragments.freelancer.FreelancerFragmentMvpPresenter;
import com.bouniane.ui.fragments.freelancer.FreelancerFragmentMvpView;
import com.bouniane.ui.fragments.freelancer.FreelancerFragmentPresenter;
import com.bouniane.ui.fragments.freelancerJobList.FreelancerJobsAdapter;
import com.bouniane.ui.fragments.freelancerJobList.FreelancerJobsFragmentMvpPresenter;
import com.bouniane.ui.fragments.freelancerJobList.FreelancerJobsFragmentMvpView;
import com.bouniane.ui.fragments.freelancerJobList.FreelancerJobsFragmentPresenter;
import com.bouniane.ui.fragments.main.MainFragmentMvpPresenter;
import com.bouniane.ui.fragments.main.MainFragmentMvpView;
import com.bouniane.ui.fragments.main.MainFragmentPresenter;
import com.bouniane.ui.fragments.otpBottomSheet.OtpDialogFragmentMvpPresenter;
import com.bouniane.ui.fragments.otpBottomSheet.OtpDialogFragmentMvpView;
import com.bouniane.ui.fragments.otpBottomSheet.OtpDialogFragmentPresenter;
import com.bouniane.utilities.AppConstants;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import rx.subscriptions.CompositeSubscription;


@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeSubscription getSubscriptions() {
        return new CompositeSubscription();
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @PerActivity
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @PerActivity
    SplashMvpPresenter<SplashMvpView> provideSplashPresenter(
            SplashPresenter<SplashMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    LoginMvpPresenter<LoginMvpView> provideLoginPresenter(
            LoginPresenter<LoginMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> provideMainPresenter(
            MainPresenter<MainMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    SignUpMvpPresenter<SignUpMvpView> provideSignUpPresenter(
            SignUpPresenter<SignUpMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    SignUpWithMvpPresenter<SignUpWithMvpView> provideSignUpWithPresenter(
            SignUpWithPresenter<SignUpWithMvpView> presenter) {
        return presenter;
    }


    /*fragment presenter*/


    @Provides
    @PerActivity
    MainFragmentMvpPresenter<MainFragmentMvpView> provideMainFragmentPresenter(
            MainFragmentPresenter< MainFragmentMvpView> presenter) {
        return presenter;
    }
    @Provides
    @PerActivity
    EmployerFragmentMvpPresenter<EmployerFragmentMvpView> provideEmployerFragmentPresenter(
            EmployerFragmentPresenter< EmployerFragmentMvpView> presenter) {
        return presenter;
    }
    @Provides
    @PerActivity
    FreelancerFragmentMvpPresenter<FreelancerFragmentMvpView> provideFreelancerFragmentPresenter(
            FreelancerFragmentPresenter< FreelancerFragmentMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    FreelancerJobsFragmentMvpPresenter<FreelancerJobsFragmentMvpView> provideFreelancerJobsFragmentPresenter(
            FreelancerJobsFragmentPresenter<  FreelancerJobsFragmentMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    OtpDialogFragmentMvpPresenter<OtpDialogFragmentMvpView> provideOtpDialogFragmentPresenter(
            OtpDialogFragmentPresenter<OtpDialogFragmentMvpView> presenter) {
        return presenter;
    }

    /* adapter */

    @Provides
    FreelancerJobsAdapter provideOfferListAdapter() {
        return new FreelancerJobsAdapter();
    }




}
