package com.bouniane.networking;

import com.bouniane.MvpApp;
import com.bouniane.model.login.LoginMaster;
import com.bouniane.prefs.AppPreferencesHelper;
import com.bouniane.utilities.AppConstants;
import com.bouniane.utilities.CommonUtils;
import com.google.gson.JsonObject;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


/**
 * Created by ennur on 6/25/16.
 */
public class NetworkService {
    private final WebServices webServices;
    private static final String TAG = NetworkService.class.getSimpleName();

    public NetworkService(WebServices webServices) {
        this.webServices = webServices;
    }

    /*---------------------------------Get Auth Token-------------------------------------------*/
    private String getAuthToken() {
        String token = "0";
        try {
            AppPreferencesHelper preferencesHelper = new AppPreferencesHelper(MvpApp.getContext(), AppConstants.PREF_NAME);
            token = preferencesHelper.getAuthToken();
        } catch (Exception e) {
            CommonUtils.pLog("log_tag", "get auth token error : " + e.getMessage());
            e.printStackTrace();
        }
        CommonUtils.pLog("log_tag", "Authorization token : " + token);
        return token;
    }

    /*---------------------------------Login-------------------------------------------*/

    public Subscription getClientLogin(final JsonObject jsonObject, final GetLoginCallback callback) {

        return webServices.loginApi(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends LoginMaster>>() {
                    @Override
                    public Observable<? extends LoginMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<LoginMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                        CommonUtils.pLog(TAG, "Network service Error : " + e.getMessage().toString());
                    }

                    @Override
                    public void onNext(LoginMaster loginMaster) {
                        callback.onSuccess(loginMaster);

                    }
                });
    }

    public interface GetLoginCallback {
        void onSuccess(LoginMaster loginMaster);

        void onError(NetworkError networkError);
    }

    /*----------------------------------------Register-----------------------------------------------*/
    public Subscription register(final JsonObject jsonObject, final GetLoginCallback callback) {

        return webServices.registerApi(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends LoginMaster>>() {
                    @Override
                    public Observable<? extends LoginMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<LoginMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                        CommonUtils.pLog(TAG, "Network service Error : " + e.getMessage().toString());
                    }

                    @Override
                    public void onNext(LoginMaster loginMaster) {
                        callback.onSuccess(loginMaster);

                    }
                });
    }


}