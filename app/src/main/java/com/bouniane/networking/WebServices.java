package com.bouniane.networking;


import com.bouniane.model.login.LoginMaster;
import com.google.gson.JsonObject;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

public interface WebServices {

    @POST(APIUrl.REGISTER)
    Observable<LoginMaster> registerApi(@Body JsonObject jsonBody);

    @POST(APIUrl.LOGIN)
    Observable<LoginMaster> loginApi(@Body JsonObject jsonBody);



}
