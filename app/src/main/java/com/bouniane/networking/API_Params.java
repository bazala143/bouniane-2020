package com.bouniane.networking;

public class API_Params {
    public static final String json_content = "json_content";
    public static String email  ="email";
    public static String userName  ="userName";
    public static String password  ="password";
    public static String phone  ="phone";
    public static String device  ="device";
    public static String deviceToken  ="deviceToken";
    public static String languageCode  ="languageCode";
    public static String isSocial  ="isSocial";
    public static String socialType  ="socialType";
    public static String image  ="image";
    public static String isEmailverified  ="isEmailverified";
    public static String isPhoneVerified  ="isPhoneVerified";
    public static String socialId = "socialId";
    public static String socialToken = "socialToken";
}
