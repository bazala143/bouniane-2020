package com.bouniane.networking;



import com.bouniane.di.PerActivity;
import com.bouniane.utilities.CommonUtils;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by ennur on 6/28/16.
 */
@Module
public class NetworkModule {
    File cacheFile;

    public NetworkModule(File cacheFile) {
        this.cacheFile = cacheFile;
    }

    @Provides
    @PerActivity
    public Retrofit provideCall() {
        Cache cache = null;
        try {
            cache = new Cache(cacheFile, 10 * 1024 * 1024);
        } catch (Exception e) {
            e.printStackTrace();
        }

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        // Customize the request
                        Request request = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .header("Connection", "close")
                                .removeHeader("Pragma")
                                /*.header("Cache-Control", String.format("max-age=%d",
                                        31536000))*/
                                .build();

                        CommonUtils.pLog("URL : ", request.url().toString() + "");
                        try {
                            final Buffer buffer = new Buffer();
                            request.body().writeTo(buffer);
                            CommonUtils.pLog("Parameter : ", buffer.readUtf8() + "");
                        } catch (final Exception e) {
                            CommonUtils.pLog("Parameter : ", "did not work");
                        }

                        okhttp3.Response response = chain.proceed(request);
                        response.cacheResponse();
//                        CommonUtils.pLog("Http Response : ", response.toString());
                        // Customize or return the response
                        return response;
                    }
                })
                .cache(cache)

                .build();


        return new Retrofit.Builder()
                .baseUrl(APIUrl.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())

                .build();
    }

    @Provides
    @PerActivity
    @SuppressWarnings("unused")
    public WebServices providesNetworkService(
            Retrofit retrofit) {
        return retrofit.create(WebServices.class);
    }

    @Provides
    @PerActivity
    @SuppressWarnings("unused")
    public NetworkService providesService(
            WebServices networkService) {
        return new NetworkService(networkService);
    }

}
