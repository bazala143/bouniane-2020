package com.bouniane.broadcastReceiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.bouniane.prefs.AppPreferencesHelper;
import com.bouniane.utilities.AppConstants;


public class BootCompletedIntentReceiver extends BroadcastReceiver {

    AppPreferencesHelper sharedPref = null;

    @Override
    public void onReceive(Context context, Intent intent) {
        sharedPref = new AppPreferencesHelper(context, AppConstants.PREF_NAME);
        if (sharedPref.getIsStarted()) {
            if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
                Intent alarmIntent = new Intent(context, AlarmReceiver.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
                AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                manager.cancel(pendingIntent);
                manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 5000, 8000, pendingIntent);
            }
        } else {
            Log.e("log_tag", "PLEASE LOGIN FIRST.");
        }
        Log.e("log_tag", "BOOT COMPLETED INVOKED.");
    }
}