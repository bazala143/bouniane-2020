package com.bouniane.broadcastReceiver;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.bouniane.di.component.DaggerServiceComponent;
import com.bouniane.networking.NetworkModule;
import com.bouniane.networking.NetworkService;
import com.bouniane.prefs.AppPreferencesHelper;
import com.bouniane.utilities.AppConstants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import org.json.JSONObject;

import java.io.File;

import javax.inject.Inject;

import rx.Subscription;

public class AlarmReceiver extends BroadcastReceiver {


    Context context;
    @Inject
    NetworkService service;
    AppPreferencesHelper sharedPref;
    boolean isFirst;
    long currTimeStamp;
    String logString;
    private static final String TAG = AlarmReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "Background service called");
        this.context = context;
        File cacheFile = new File(context.getCacheDir(), "responses");
        DaggerServiceComponent.builder()
                .networkModule(new NetworkModule(cacheFile))
                .build()
                .injectAlarmReceiver(this);
        sharedPref = new AppPreferencesHelper(context, AppConstants.PREF_NAME);
        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 8000, 8000, pendingIntent);
        // For our recurring task, we'll just display a message
        //Log.e("sendlocation", "Every 5 second it will appear in Log Console");
        isFirst = true;

    }

}