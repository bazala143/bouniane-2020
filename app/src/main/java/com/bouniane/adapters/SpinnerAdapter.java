package com.bouniane.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.bouniane.R;

import java.util.List;

public class SpinnerAdapter extends BaseAdapter {
    LayoutInflater inflter;
    List<String> titleList;

    public SpinnerAdapter(List<String> titleList) {
        this.titleList = titleList;
    }

    @Override
    public int getCount() {
        return titleList.size();
    }

    @Override
    public Object getItem(int i) {
        return titleList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public void addItems(List<String> titleList) {
        this.titleList.addAll(titleList);
        notifyDataSetChanged();
    }

    public List<String> getAllItems() {
        return this.titleList;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (inflter == null) {
            inflter = (LayoutInflater.from(viewGroup.getContext()));
        }
        view = inflter.inflate(R.layout.layout_spinner_adapter, null, false);

        return view;
    }


}