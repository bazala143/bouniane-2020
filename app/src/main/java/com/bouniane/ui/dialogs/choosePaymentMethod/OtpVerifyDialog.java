package com.bouniane.ui.dialogs.choosePaymentMethod;

import android.app.Activity;
import android.content.IntentFilter;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.bouniane.R;
import com.bouniane.broadcastReceiver.MySMSBroadcastReceiver;
import com.bouniane.databinding.LayoutOtpDialogBinding;
import com.bouniane.utilities.CommonUtils;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;


public class OtpVerifyDialog implements MySMSBroadcastReceiver.OTPReceiveListener {
    Activity activity;
    MySMSBroadcastReceiver mySMSBroadcastReceiver;

    public OtpVerifyDialog() {

    }

    BottomSheetDialog dialog;
    OtpVerifyDialogClickListener dialogClickListener;
    LayoutOtpDialogBinding binding;

    private void startRegister() {
        mySMSBroadcastReceiver = new MySMSBroadcastReceiver();
        mySMSBroadcastReceiver.setOTPListener(this);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
        activity.registerReceiver(mySMSBroadcastReceiver, intentFilter);

        SmsRetrieverClient client = SmsRetriever.getClient(activity);
        Task<Void> task = client.startSmsRetriever();
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
             binding.btnLogin.setVisibility(View.GONE);
             binding.progressbar.setVisibility(View.VISIBLE);
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                binding.btnLogin.setVisibility(View.VISIBLE);
                binding.progressbar.setVisibility(View.GONE);
            }
        });
    }

    public void showDialog(Activity activity, OtpVerifyDialogClickListener listener) {

        try {
            this.activity = activity;
            startRegister();
            dialogClickListener = listener;
            dialog = new BottomSheetDialog(activity);
            LayoutInflater layoutInflater = activity.getLayoutInflater();
            binding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_otp_dialog, null, false);
            dialog.setContentView(binding.getRoot());
            binding.btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    dialogClickListener.onVerifyClicked();
                }
            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtils.pLog("dialog", e.getMessage());
        }
    }


    public void dismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void onOTPReceived(String otp) {
        if (mySMSBroadcastReceiver != null) {
            activity.unregisterReceiver(mySMSBroadcastReceiver);
            mySMSBroadcastReceiver = null;
        }
        binding.enterOtp.setText(otp);
    }

    @Override
    public void onOTPTimeOut() {

    }

    @Override
    public void onOTPReceivedError(String error) {

    }
}
