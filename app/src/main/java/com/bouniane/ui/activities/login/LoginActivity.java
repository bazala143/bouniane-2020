package com.bouniane.ui.activities.login;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.bouniane.R;
import com.bouniane.base.BaseActivity;
import com.bouniane.databinding.ActivityLoginBinding;
import com.bouniane.databinding.ActivitySplashBinding;
import com.bouniane.model.login.requestModels.LoginRequest;
import com.bouniane.ui.activities.main.MainActivity;
import com.bouniane.ui.activities.signUpWith.SignUpWithActivity;
import com.bouniane.ui.activities.signup.SignUpActivity;
import com.bouniane.ui.fragments.main.MainFragment;
import com.bouniane.utilities.AppConstants;
import com.bouniane.utilities.LocaleHelper;
import com.bouniane.utilities.RequestCodes;
import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.inject.Inject;


public class LoginActivity extends BaseActivity implements LoginMvpView, View.OnClickListener {

    ActivityLoginBinding binding;
    @Inject
    LoginMvpPresenter<LoginMvpView> mPresenter;
    private static final String TAG = LoginActivity.class.getSimpleName();
    CallbackManager callbackManager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        mPresenter.onAttach(this);
        binding.setClickListener(this);
        mPresenter.setBindings(binding);
        callbackManager = mPresenter.getFacebookCallback();
        printHashKey(LoginActivity.this);
    }



    public static void printHashKey(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.d("Hash Key:", hashKey);
            }
        } catch (Exception e) {
            Log.e(TAG, "printHashKey()", e);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCodes.REQUEST_GOOGLE_LOGIN) {
            mPresenter.sighoutfromGoogle(LoginActivity.this, requestCode, data);
        }
    }


    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        if (view == binding.btnGoogle) {
            mPresenter.loginWithGoogle(LoginActivity.this);
        } else if (view == binding.btnFacebook) {
            mPresenter.loginWithFacebook(LoginActivity.this);
        } else if (view == binding.btnApple) {
            // apple login
            mPresenter.loginWihApple(LoginActivity.this);
        } else if (view == binding.btnLogin) {
            if (mPresenter.isValidData()) {
                LoginRequest loginRequest = new LoginRequest();
                loginRequest.setEmail(binding.edtPhoneNumber.getText().toString().trim());
                loginRequest.setPassword(binding.edtPassword.getText().toString());
                loginRequest.setIsSocial(AppConstants.NOT_SOCIAL);
                mPresenter.loginApiCall(loginRequest);
            }
        } else if (view == binding.btnRegister) {
            Intent i = new Intent(LoginActivity.this, SignUpWithActivity.class);
            startActivity(i);
        }
    }

    @Override
    public void redirectToMain() {


    }
}
