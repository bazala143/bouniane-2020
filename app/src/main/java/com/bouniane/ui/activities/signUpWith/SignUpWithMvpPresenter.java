/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.bouniane.ui.activities.signUpWith;


import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;

import com.bouniane.base.MvpPresenter;
import com.bouniane.databinding.ActivityLoginBinding;
import com.bouniane.databinding.ActivitySignupwithBinding;
import com.bouniane.di.PerActivity;
import com.bouniane.model.login.requestModels.LoginRequest;
import com.facebook.CallbackManager;


@PerActivity
public interface SignUpWithMvpPresenter<V extends SignUpWithMvpView> extends MvpPresenter<V> {

    void sendverificationCode(String toString);

    void setActivity(Activity activity);

    void loginWithFacebook(Activity activity);

    void loginWithGoogle(Activity activity);

    void sighoutfromGoogle(Activity activity, int requestCode, Intent data);

    void loginWihApple(V loginActivity);

    boolean isValidData();

    void loginApiCall(LoginRequest loginRequest);

    CallbackManager getFacebookCallback();

    void setBindings(ActivitySignupwithBinding binding);

    void  requestPhoneNuumbers(Activity activity) throws IntentSender.SendIntentException;
}
