package com.bouniane.ui.activities.splash;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.bouniane.base.BasePresenter;
import com.bouniane.networking.NetworkService;
import com.bouniane.prefs.AppPreferencesHelper;
import com.bouniane.utilities.CommonUtils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class SplashPresenter<V extends SplashMvpView> extends BasePresenter<V>
        implements SplashMvpPresenter<V> {
    @Inject
    NetworkService service;
    Handler handler;
    private static final int SPLASH_DURATION = 2000;
    private static final String TAG = SplashPresenter.class.getSimpleName();


    @Inject
    public SplashPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);

    }

    @Override
    public void onNextActivity() {
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
            getMvpView().redirectToHome();
            }
        }, 2000);

    }

    @Override
    public void storeLocation(String latitude, String longitude) {

    }

    @Override
    public boolean isStarted() {
        return getPreferencesHelper().getIsStarted();
    }


    @Override
    public void checkAppVersion(String currentVersion, String packageName) {

        ForceUpdateAsync forceUpdateAsync = new ForceUpdateAsync(String.valueOf(currentVersion), packageName);
        forceUpdateAsync.execute();
    }

    @Override
    public void setLanguageCode(String language) {

    }

    /*Fetch Play Store Version*/
    public class ForceUpdateAsync extends AsyncTask<String, String, JSONObject> {

        private String playStoreVersion;
        private String currentVersion;
        private String packageName;

        public ForceUpdateAsync(String currentVersion, String packageName) {
            this.currentVersion = currentVersion;
            this.packageName = packageName;
        }

        @Override
        protected JSONObject doInBackground(String... params) {

            try {
                playStoreVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + packageName + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            CommonUtils.pLog(TAG, "Current version : " + currentVersion);
            CommonUtils.pLog(TAG, "play store Version : " + playStoreVersion);
            if (playStoreVersion != null && !currentVersion.equalsIgnoreCase(playStoreVersion)) {
                //   getMvpView().openForceUpdateDialog();
            } else {
                //  getMvpView().readWritePermission();
            }
            super.onPostExecute(jsonObject);
        }
    }
}
