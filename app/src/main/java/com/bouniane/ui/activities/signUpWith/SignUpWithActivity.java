package com.bouniane.ui.activities.signUpWith;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.bouniane.R;
import com.bouniane.base.BaseActivity;
import com.bouniane.broadcastReceiver.MySMSBroadcastReceiver;
import com.bouniane.databinding.ActivitySignupBinding;
import com.bouniane.databinding.ActivitySignupwithBinding;
import com.bouniane.model.login.requestModels.LoginRequest;
import com.bouniane.ui.activities.login.LoginActivity;
import com.bouniane.ui.dialogs.choosePaymentMethod.OtpVerifyDialog;
import com.bouniane.ui.dialogs.choosePaymentMethod.OtpVerifyDialogClickListener;
import com.bouniane.ui.fragments.otpBottomSheet.OtpDialogFragment;
import com.bouniane.utilities.AppConstants;
import com.bouniane.utilities.RequestCodes;
import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.credentials.Credential;

import javax.inject.Inject;


public class SignUpWithActivity extends BaseActivity implements SignUpWithMvpView, View.OnClickListener, MySMSBroadcastReceiver.OTPReceiveListener {

    ActivitySignupwithBinding binding;
    @Inject
    SignUpWithMvpPresenter<SignUpWithMvpView> mPresenter;
    private static final String TAG = SignUpWithActivity.class.getSimpleName();
    CallbackManager callbackManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signupwith);
        binding.setClickListener(this);
        mPresenter.onAttach(this);
        mPresenter.setBindings(binding);
        callbackManager = mPresenter.getFacebookCallback();
        binding.edtPhoneNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    try {
                        mPresenter.requestPhoneNuumbers(SignUpWithActivity.this);
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }


    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCodes.REQUEST_GOOGLE_LOGIN) {
            mPresenter.sighoutfromGoogle(SignUpWithActivity.this, requestCode, data);
        } else if (requestCode == RequestCodes.PHONE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
                if (credential != null) {
                    String mobNumber = credential.getId();
                    String newString = mobNumber.replace("+91", "");
                    binding.edtPhoneNumber.setText(newString);
                    mPresenter.sendverificationCode(newString);
                } else {
                    Toast.makeText(this, "err", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    @Override
    public void onClick(View view) {
        if (view == binding.btnSend) {
            if (binding.edtPhoneNumber.getText().toString().isEmpty()) {
                binding.edtPhoneNumber.setError(getString(R.string.please_enter_phone));
            } else if (binding.edtPhoneNumber.getText().toString().length() == 10) {
                mPresenter.sendverificationCode(binding.edtPhoneNumber.getText().toString());
            }
        } else if (view == binding.btnGoogle) {
            mPresenter.loginWithGoogle(SignUpWithActivity.this);
        } else if (view == binding.btnFacebook) {
            mPresenter.loginWithFacebook(SignUpWithActivity.this);
        } else if (view == binding.btnApple) {
            // apple login
            mPresenter.loginWihApple(SignUpWithActivity.this);
        } else if (view == binding.btnLogin) {
            Intent i = new Intent(SignUpWithActivity.this, LoginActivity.class);
            startActivity(i);
        }
    }

    @Override
    public void onCodeSent() {
        OtpVerifyDialog otpVerifyDialog = new OtpVerifyDialog();
        otpVerifyDialog.showDialog(SignUpWithActivity.this, new OtpVerifyDialogClickListener() {
            @Override
            public void onVerifyClicked() {

            }
        });
    }

    @Override
    public void onOTPReceived(String otp) {

    }

    @Override
    public void onOTPTimeOut() {

    }

    @Override
    public void onOTPReceivedError(String error) {

    }
}
