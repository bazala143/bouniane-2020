package com.bouniane.ui.activities.main;

import android.os.AsyncTask;
import android.os.Handler;

import com.bouniane.base.BasePresenter;
import com.bouniane.networking.NetworkService;
import com.bouniane.prefs.AppPreferencesHelper;
import com.bouniane.utilities.CommonUtils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONObject;
import org.jsoup.Jsoup;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V>
        implements MainMvpPresenter<V> {
    @Inject
    NetworkService service;



    @Inject
    public MainPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);

    }



}
