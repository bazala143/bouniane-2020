package com.bouniane.ui.activities.splash;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;


import com.bouniane.R;
import com.bouniane.base.BaseActivity;
import com.bouniane.databinding.ActivitySplashBinding;
import com.bouniane.ui.activities.login.LoginActivity;
import com.bouniane.ui.activities.main.MainActivity;
import com.bouniane.utilities.AppConstants;
import com.bouniane.utilities.LocaleHelper;

import javax.inject.Inject;


public class SplashActivity extends BaseActivity implements SplashMvpView {

    ActivitySplashBinding binding;
    @Inject
    SplashMvpPresenter<SplashMvpView> mPresenter;
    private static final String TAG = SplashActivity.class.getSimpleName();
    private static final int REQUEST = 112;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        mPresenter.onAttach(this);
        mPresenter.setLanguageCode(AppConstants.LANGUAGE_ENGLISH);
        LocaleHelper.setLocale(SplashActivity.this, AppConstants.LANGUAGE_ENGLISH);
        mPresenter.onNextActivity();
    }


    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void redirectToHome() {
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
        SplashActivity.this.finish();
     }
}
