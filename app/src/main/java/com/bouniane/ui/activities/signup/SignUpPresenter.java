package com.bouniane.ui.activities.signup;

import com.bouniane.base.BasePresenter;
import com.bouniane.networking.NetworkService;
import com.bouniane.prefs.AppPreferencesHelper;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

public class SignUpPresenter<V extends SignUpMvpView> extends BasePresenter<V>
        implements SignUpMvpPresenter<V> {
    @Inject
    NetworkService service;

    @Inject
    public SignUpPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);

    }


}
