package com.bouniane.ui.activities.signup;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import com.bouniane.R;
import com.bouniane.base.BaseActivity;
import com.bouniane.databinding.ActivityLoginBinding;
import com.bouniane.databinding.ActivitySignupBinding;

import javax.inject.Inject;


public class SignUpActivity extends BaseActivity implements SignUpMvpView {

    ActivitySignupBinding binding;
    @Inject
    SignUpMvpPresenter<SignUpMvpView> mPresenter;
    private static final String TAG = SignUpActivity.class.getSimpleName();
    private static final int REQUEST = 112;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup);
        mPresenter.onAttach(this);

    }


    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}
