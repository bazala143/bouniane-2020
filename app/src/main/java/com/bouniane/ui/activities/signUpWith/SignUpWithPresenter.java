package com.bouniane.ui.activities.signUpWith;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bouniane.R;
import com.bouniane.base.BasePresenter;
import com.bouniane.databinding.ActivitySignupwithBinding;
import com.bouniane.model.login.LoginMaster;
import com.bouniane.model.login.requestModels.LoginRequest;
import com.bouniane.networking.NetworkError;
import com.bouniane.networking.NetworkService;
import com.bouniane.prefs.AppPreferencesHelper;
import com.bouniane.ui.activities.signup.SignUpActivity;
import com.bouniane.ui.fragments.otpBottomSheet.OtpDialogFragment;
import com.bouniane.utilities.AppConstants;
import com.bouniane.utilities.RequestCodes;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class SignUpWithPresenter<V extends SignUpWithMvpView> extends BasePresenter<V>
        implements SignUpWithMvpPresenter<V> {
    @Inject
    NetworkService service;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    String mVerificationId;
    Activity activity;
    private FirebaseAuth mAuth;
    CallbackManager callbackManager;
    GoogleSignInOptions gso;
    GoogleSignInClient mGoogleSignInClient;
    GoogleApiClient googleApiClient;
    private int RC_SIGN_IN = 111;
    private String TAG = this.getClass().getSimpleName();
    ActivitySignupwithBinding binding;

    @Inject
    public SignUpWithPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);

    }


    @Override
    public void sendverificationCode(String mobile) {
        getMvpView().showLoading();
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                getMvpView().hideLoading();
                getMvpView().onCodeSent();

            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                getMvpView().hideLoading();

            }

            @Override
            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                getMvpView().hideLoading();

                // show dialog
            }
        };

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                AppConstants.PHONE_CODE + mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);

    }


    @Override
    public void setActivity(Activity activity) {
        this.activity = activity;
        mAuth = FirebaseAuth.getInstance();
    }

    private void verifyVerificationCode(String otp) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, otp);
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Intent intent = new Intent(activity, SignUpActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            activity.startActivity(intent);
                        } else {
                            Toast.makeText(activity, "Failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void loginWithFacebook(Activity activity) {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().setLoginBehavior(LoginBehavior.WEB_VIEW_ONLY);
    }


    @Override
    public void loginWithGoogle(Activity activity) {
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(activity.getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(activity, gso);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        activity.startActivityForResult(signInIntent, RequestCodes.REQUEST_GOOGLE_LOGIN);
    }

    @Override
    public void sighoutfromGoogle(Activity activity, int requestCode, Intent data) {
        if (requestCode == RequestCodes.REQUEST_GOOGLE_LOGIN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                LoginRequest loginRequest = new LoginRequest();
                loginRequest.setEmail(account.getEmail());
                loginRequest.setUserName(account.getDisplayName());
                loginRequest.setImage(account.getPhotoUrl().toString());
                loginRequest.setSocialToken(account.getIdToken());
                loginRequest.setSocialId(account.getId());
                loginRequest.setSocialType(AppConstants.TYPE_GOOGLE);
                loginRequest.setIsEmailverified(AppConstants.VERIFIED);
                loginRequest.setIsPhoneVerified(AppConstants.NOT_VERIFIED);
                loginRequest.setIsSocial(AppConstants.SOCIAL);
                loginApiCall(loginRequest);
                mGoogleSignInClient.signOut()
                        .addOnCompleteListener(activity, new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                            }
                        });

            } catch (ApiException e) {
                Log.w(TAG, "Google sign in failed", e);
            }
        }


    }

    @Override
    public void loginWihApple(V loginActivity) {

    }

    @Override
    public boolean isValidData() {
        if (binding.edtPhoneNumber.getText().toString().isEmpty()) {
            binding.edtPhoneNumber.setError(getMvpView().getStringFromId(R.string.please_enter_email_address));
            return false;
        }
        return true;
    }

    @Override
    public void loginApiCall(LoginRequest loginRequest) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription subscription = service.getClientLogin(loginParams(loginRequest), new NetworkService.GetLoginCallback() {
                @Override
                public void onSuccess(LoginMaster loginMaster) {
                    getMvpView().hideLoading();
                    Log.e("Response", new Gson().toJson(loginMaster));
                    if (loginMaster != null) {
                        if (loginMaster.getSuccess() == 1) {
                            getPreferencesHelper().setLoginDetails(new Gson().toJson(loginMaster.getLoginDetails()));
                            getMvpView().showMessage(loginMaster.getMessage());
                        } else {
                            getMvpView().showMessage(loginMaster.getMessage());
                        }
                    }
                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().onError(networkError.getMessage() + "");
                }
            });
            getSubscription().add(subscription);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }

    @Override
    public CallbackManager getFacebookCallback() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("LoginActivity", response.toString());
                                        try {
                                            String email = object.optString("email");
                                            String name = object.getString("name");
                                            String id = object.getString("id");
                                            String image_url = "http://graph.facebook.com/" + id + "/picture?type=large";
                                            if (email != null) {
                                                LoginRequest loginRequest = new LoginRequest();
                                                loginRequest.setEmail(email);
                                                loginRequest.setUserName(name);
                                                loginRequest.setImage(image_url);
                                                loginRequest.setSocialToken(loginResult.getAccessToken().getToken());
                                                loginRequest.setSocialId(id);
                                                loginRequest.setSocialType(AppConstants.TYPE_FACEBOOK);
                                                loginRequest.setIsEmailverified(AppConstants.VERIFIED);
                                                loginRequest.setIsPhoneVerified(AppConstants.NOT_VERIFIED);
                                                loginRequest.setIsSocial(AppConstants.SOCIAL);
                                                loginApiCall(loginRequest);
                                            } else {
                                                getMvpView().showMessage("unable to get message");
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();


                    }

                    @Override
                    public void onCancel() {
                        getMvpView().showMessage("Login cancelled");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        getMvpView().showMessage(exception.toString());

                    }
                });

        return callbackManager;
    }

    @Override
    public void setBindings(ActivitySignupwithBinding binding) {
        this.binding = binding;
    }

    @Override
    public void requestPhoneNuumbers(Activity activity) throws IntentSender.SendIntentException {
        googleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {

                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                }).addApi(Auth.CREDENTIALS_API)
                .build();


        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();

        PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(
                googleApiClient, hintRequest);
        activity.startIntentSenderForResult(intent.getIntentSender(),
                RequestCodes.PHONE_REQUEST_CODE, null, 0, 0, 0);
    }

    private JsonObject loginParams(LoginRequest loginRequest) {
        loginRequest.setDevice(AppConstants.DEVICE_TYPE);
        loginRequest.setDeviceToken(getPreferencesHelper().getDeviceToken());
        loginRequest.setLanguageCode(getPreferencesHelper().getLanguageCode());
      /*  JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.email, binding.edtPhoneNumber.getText());
            jsonObject.put(API_Params.userName, binding.edtPassword.getText());
            jsonObject.put(API_Params.deviceToken, getPreferencesHelper().getDeviceToken());
            jsonObject.put(API_Params.device, AppConstants.DEVICE_TYPE);
            jsonObject.put(API_Params.isSocial, "0");
            JsonElement jsonElement = JsonParser.parseString(jsonObject.toString());
            gsonObject = jsonElement.getAsJsonObject();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("loginParams", gsonObject.toString());*/
        Log.e("loginParams", new GsonBuilder().create().toJson(loginRequest));
        return new GsonBuilder().create().toJsonTree(loginRequest).getAsJsonObject();
    }

   /* private JsonObject socialLoginParams(String email, String userName, String socialId, String image, String socialToken, String socialType, String isSocial, String password) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.email, email);
            if (userName != null) {
                jsonObject.put(API_Params.userName, userName);
            }
            if (image != null) {
                jsonObject.put(API_Params.image, image != null ? image : "");
            }
            if (socialId != null) {
                jsonObject.put(API_Params.socialId, socialId);
            }
            jsonObject.put(API_Params.deviceToken, getPreferencesHelper().getDeviceToken());
            jsonObject.put(API_Params.device, AppConstants.DEVICE_TYPE);
            jsonObject.put(API_Params.isSocial, isSocial);
            if (socialType != null) {
                jsonObject.put(API_Params.socialType, socialType);
            }
            if (socialToken != null) {
                jsonObject.put(API_Params.socialToken, socialToken);
            }
            if (password != null) {
                jsonObject.put(API_Params.password, password);

            }
            JsonElement jsonElement = JsonParser.parseString(jsonObject.toString());
            gsonObject = jsonElement.getAsJsonObject();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("loginParams", gsonObject.toString());
        return gsonObject;
    }
*/


}