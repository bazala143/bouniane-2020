package com.bouniane.ui.activities.main;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bouniane.R;
import com.bouniane.base.BaseActivity;
import com.bouniane.databinding.ActivityMainBinding;
import com.bouniane.databinding.ActivitySplashBinding;
import com.bouniane.ui.fragments.main.MainFragment;
import com.bouniane.utilities.AppConstants;
import com.bouniane.utilities.CommonUtils;
import com.bouniane.utilities.LocaleHelper;

import javax.inject.Inject;


public class MainActivity extends BaseActivity implements MainMvpView {

    ActivityMainBinding binding;
    @Inject
    MainMvpPresenter<MainMvpView> mPresenter;
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        mPresenter.onAttach(this);
        loadFragment(MainFragment.newInstance());

    }

    public void loadFragment(Fragment fragment) {
        String backState = fragment.getClass().getSimpleName();
        String fragmentTag = backState;
        CommonUtils.pLog(TAG, fragment.getClass().getSimpleName().toString());
        FragmentManager fragmentManager = getSupportFragmentManager();
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backState, 0);

        if (!fragmentPopped && fragmentManager.findFragmentByTag(fragmentTag) == null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment, fragmentTag);
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            fragmentTransaction.addToBackStack(backState);
            fragmentTransaction.commit();
        }
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}
