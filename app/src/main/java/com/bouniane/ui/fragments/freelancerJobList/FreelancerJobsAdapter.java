package com.bouniane.ui.fragments.freelancerJobList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bouniane.R;
import com.bouniane.base.BaseViewHolder;
import com.bouniane.databinding.AdapterFreelancerJobsBinding;

import java.util.List;


public class FreelancerJobsAdapter extends RecyclerView.Adapter<BaseViewHolder> implements View.OnClickListener {
    private List<String> mList;
    LayoutInflater layoutInflater;

    public FreelancerJobsAdapter() {
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        AdapterFreelancerJobsBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.adapter_freelancer_jobs, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    @Override
    public void onClick(View v) {

    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends BaseViewHolder {
        AdapterFreelancerJobsBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public ViewHolder(final AdapterFreelancerJobsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        protected void clear() {

        }

        public void onBind(final int position) {
            super.onBind(position);

        }


    }

    public void addAll(List<String> productCategoryList) {
        this.mList = productCategoryList;
        notifyDataSetChanged();
    }



}

