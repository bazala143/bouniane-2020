package com.bouniane.ui.fragments.freelancer;

import com.bouniane.base.BasePresenter;
import com.bouniane.networking.NetworkService;
import com.bouniane.prefs.AppPreferencesHelper;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;


public class FreelancerFragmentPresenter<V extends FreelancerFragmentMvpView> extends BasePresenter<V>
        implements FreelancerFragmentMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = FreelancerFragmentPresenter.class.getSimpleName();

    @Inject
    public FreelancerFragmentPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);

    }

    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }
}
