package com.bouniane.ui.fragments.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentStatePagerAdapter;


import com.bouniane.R;
import com.bouniane.base.BaseFragment;
import com.bouniane.databinding.FragmentMainBinding;
import com.bouniane.databinding.TabItemBinding;
import com.bouniane.di.component.ActivityComponent;
import com.bouniane.ui.activities.login.LoginActivity;
import com.bouniane.ui.fragments.employer.EmployerFragment;
import com.bouniane.ui.fragments.freelancer.FreelancerFragment;
import com.google.android.material.tabs.TabLayout;

import javax.inject.Inject;


public class MainFragment extends BaseFragment implements
        MainFragmentMvpView, View.OnClickListener {

    private static final String TAG = MainFragment.class.getSimpleName();
    MainFragmentAdapter mainFragmentAdapter;

    @Inject
    MainFragmentMvpPresenter<MainFragmentMvpView> mPresenter;
    FragmentMainBinding binding;

    public static MainFragment newInstance() {
        Bundle args = new Bundle();
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);
        binding.cToolbar.tvTitle.setText("My Jobs");
        binding.cToolbar.ivProfile.setImageResource(R.drawable.img_hayat);
        binding.cToolbar.ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getBaseActivity(), LoginActivity.class);
                startActivity(i);
            }
        });
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }
        return binding.getRoot();
    }

    @Override
    protected void setUp(View view) {
        binding.setClickListener(this);
        mainFragmentAdapter = new MainFragmentAdapter(getChildFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mainFragmentAdapter.addFragment(new FreelancerFragment(), "Freelancer");
        mainFragmentAdapter.addFragment(new EmployerFragment(), "Employer");
        mainFragmentAdapter.setContext(getContext());
        binding.viewPager.setAdapter(mainFragmentAdapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        highLightCurrentTab(0);

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                highLightCurrentTab(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void highLightCurrentTab(int position) {
        for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = binding.tabLayout.getTabAt(i);
            assert tab != null;
            tab.setCustomView(null);
            tab.setCustomView(mainFragmentAdapter.getTabView(i));
        }
        TabLayout.Tab tab = binding.tabLayout.getTabAt(position);
        assert tab != null;
        tab.setCustomView(null);
        tab.setCustomView(mainFragmentAdapter.getSelectedTabView(position));
    }


    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}
