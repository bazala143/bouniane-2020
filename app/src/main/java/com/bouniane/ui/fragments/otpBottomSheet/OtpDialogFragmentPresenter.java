package com.bouniane.ui.fragments.otpBottomSheet;

import com.bouniane.base.BasePresenter;
import com.bouniane.networking.NetworkService;
import com.bouniane.prefs.AppPreferencesHelper;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;


public class OtpDialogFragmentPresenter<V extends OtpDialogFragmentMvpView> extends BasePresenter<V>
        implements OtpDialogFragmentMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = OtpDialogFragmentPresenter.class.getSimpleName();

    @Inject
    public OtpDialogFragmentPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);

    }

    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }
}
