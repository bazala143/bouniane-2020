package com.bouniane.ui.fragments.employer;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.bouniane.R;
import com.bouniane.base.BaseFragment;
import com.bouniane.databinding.FragmentEmployerBinding;
import com.bouniane.databinding.FragmentMainBinding;
import com.bouniane.di.component.ActivityComponent;

import javax.inject.Inject;


public class EmployerFragment extends BaseFragment implements
        EmployerFragmentMvpView, View.OnClickListener {

    private static final String TAG = EmployerFragment.class.getSimpleName();
    EmployerFragmentAdapter mainFragmentAdapter;

    @Inject
    EmployerFragmentMvpPresenter<EmployerFragmentMvpView> mPresenter;
    FragmentEmployerBinding binding;

    public static EmployerFragment newInstance() {
        Bundle args = new Bundle();
        EmployerFragment fragment = new EmployerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_employer, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }
        return binding.getRoot();
    }

    @Override
    protected void setUp(View view) {




    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}
