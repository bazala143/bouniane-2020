package com.bouniane.ui.fragments.employer;


import com.bouniane.base.MvpPresenter;

public interface EmployerFragmentMvpPresenter<V extends EmployerFragmentMvpView>
        extends MvpPresenter<V> {

}


