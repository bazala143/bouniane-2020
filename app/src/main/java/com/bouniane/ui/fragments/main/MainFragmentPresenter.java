package com.bouniane.ui.fragments.main;

import com.bouniane.base.BasePresenter;
import com.bouniane.networking.NetworkService;
import com.bouniane.prefs.AppPreferencesHelper;


import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;


public class MainFragmentPresenter<V extends MainFragmentMvpView> extends BasePresenter<V>
        implements MainFragmentMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = MainFragmentPresenter.class.getSimpleName();

    @Inject
    public MainFragmentPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);

    }

    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }
}
