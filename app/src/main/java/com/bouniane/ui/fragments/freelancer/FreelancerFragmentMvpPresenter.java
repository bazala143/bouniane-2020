package com.bouniane.ui.fragments.freelancer;


import com.bouniane.base.MvpPresenter;

public interface FreelancerFragmentMvpPresenter<V extends FreelancerFragmentMvpView>
        extends MvpPresenter<V> {

}


