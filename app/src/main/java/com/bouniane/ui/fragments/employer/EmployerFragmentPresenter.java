package com.bouniane.ui.fragments.employer;

import com.bouniane.base.BasePresenter;
import com.bouniane.networking.NetworkService;
import com.bouniane.prefs.AppPreferencesHelper;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;


public class EmployerFragmentPresenter<V extends EmployerFragmentMvpView> extends BasePresenter<V>
        implements EmployerFragmentMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = EmployerFragmentPresenter.class.getSimpleName();

    @Inject
    public EmployerFragmentPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);

    }

    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }
}
