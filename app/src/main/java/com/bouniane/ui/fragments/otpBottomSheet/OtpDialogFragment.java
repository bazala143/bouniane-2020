package com.bouniane.ui.fragments.otpBottomSheet;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.bouniane.R;
import com.bouniane.base.BaseDialog;
import com.bouniane.databinding.FragmentEmployerBinding;
import com.bouniane.databinding.LayoutOtpDialogBinding;
import com.bouniane.di.component.ActivityComponent;

import javax.inject.Inject;


public class OtpDialogFragment extends BaseDialog implements
        OtpDialogFragmentMvpView, View.OnClickListener {

    public static final String TAG = OtpDialogFragment.class.getSimpleName();

    @Inject
    OtpDialogFragmentMvpPresenter<OtpDialogFragmentMvpView> mPresenter;
    LayoutOtpDialogBinding binding;

    public static OtpDialogFragment newInstance() {
        Bundle args = new Bundle();
        OtpDialogFragment fragment = new OtpDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.layout_otp_dialog, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }
        return binding.getRoot();
    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}
