package com.bouniane.ui.fragments.freelancerJobList;


import com.bouniane.base.MvpPresenter;

public interface FreelancerJobsFragmentMvpPresenter<V extends FreelancerJobsFragmentMvpView>
        extends MvpPresenter<V> {

}


