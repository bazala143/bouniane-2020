package com.bouniane.ui.fragments.main;


import com.bouniane.base.MvpPresenter;

public interface MainFragmentMvpPresenter<V extends MainFragmentMvpView>
        extends MvpPresenter<V> {

}


