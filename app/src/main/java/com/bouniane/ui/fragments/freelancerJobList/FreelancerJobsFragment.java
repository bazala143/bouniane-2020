package com.bouniane.ui.fragments.freelancerJobList;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bouniane.R;
import com.bouniane.base.BaseFragment;
import com.bouniane.databinding.FragmentFreelancerBinding;
import com.bouniane.databinding.FragmentFreelancerJobsBinding;
import com.bouniane.di.component.ActivityComponent;

import javax.inject.Inject;


public class FreelancerJobsFragment extends BaseFragment implements
        FreelancerJobsFragmentMvpView, View.OnClickListener {

    private static final String TAG = FreelancerJobsFragment.class.getSimpleName();

    @Inject
    FreelancerJobsFragmentMvpPresenter<FreelancerJobsFragmentMvpView> mPresenter;

    @Inject
    FreelancerJobsAdapter jobsAdapter;
    LinearLayoutManager linearLayoutManager;
    FragmentFreelancerJobsBinding binding;

    public static FreelancerJobsFragment newInstance() {
        Bundle args = new Bundle();
        FreelancerJobsFragment fragment = new FreelancerJobsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_freelancer_jobs, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }
        return binding.getRoot();
    }

    @Override
    protected void setUp(View view) {
    linearLayoutManager = new LinearLayoutManager(getContext());
    binding.jobsRecyclerView.setLayoutManager(linearLayoutManager);
    binding.jobsRecyclerView.setAdapter(jobsAdapter);


    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}
