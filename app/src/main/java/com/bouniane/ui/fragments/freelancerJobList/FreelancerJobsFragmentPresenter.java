package com.bouniane.ui.fragments.freelancerJobList;

import com.bouniane.base.BasePresenter;
import com.bouniane.networking.NetworkService;
import com.bouniane.prefs.AppPreferencesHelper;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;


public class FreelancerJobsFragmentPresenter<V extends FreelancerJobsFragmentMvpView> extends BasePresenter<V>
        implements FreelancerJobsFragmentMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = FreelancerJobsFragmentPresenter.class.getSimpleName();

    @Inject
    public FreelancerJobsFragmentPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);

    }

    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }
}
