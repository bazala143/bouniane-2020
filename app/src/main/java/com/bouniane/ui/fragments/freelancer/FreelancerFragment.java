package com.bouniane.ui.fragments.freelancer;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.bouniane.R;
import com.bouniane.base.BaseFragment;
import com.bouniane.databinding.FragmentFreelancerBinding;
import com.bouniane.databinding.FragmentMainBinding;
import com.bouniane.di.component.ActivityComponent;
import com.bouniane.ui.fragments.freelancerJobList.FreelancerJobsFragment;
import com.google.android.material.tabs.TabLayout;

import javax.inject.Inject;


public class FreelancerFragment extends BaseFragment implements
        FreelancerFragmentMvpView, View.OnClickListener {

    private static final String TAG = FreelancerFragment.class.getSimpleName();
    FreelancerFragmentAdapter mainFragmentAdapter;

    @Inject
    FreelancerFragmentMvpPresenter<FreelancerFragmentMvpView> mPresenter;
    FragmentFreelancerBinding binding;

    public static FreelancerFragment newInstance() {
        Bundle args = new Bundle();
        FreelancerFragment fragment = new FreelancerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_freelancer, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }
        return binding.getRoot();
    }

    @Override
    protected void setUp(View view) {
        mainFragmentAdapter = new FreelancerFragmentAdapter(getChildFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mainFragmentAdapter.addFragment(new FreelancerJobsFragment(), "All");
        mainFragmentAdapter.addFragment(new FreelancerJobsFragment(), "Open");
        mainFragmentAdapter.addFragment(new FreelancerJobsFragment(), "Running");
        mainFragmentAdapter.addFragment(new FreelancerJobsFragment(), "Past");
        binding.viewPager.setAdapter(mainFragmentAdapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);

        mainFragmentAdapter.setContext(getContext());
        binding.viewPager.setAdapter(mainFragmentAdapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        highLightCurrentTab(0);
        for(int i=0; i < binding.tabLayout.getTabCount() - 1; i++) {
            View tab = ((ViewGroup) binding.tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            p.setMargins(0, 0, 10, 0);
            tab.requestLayout();
        }

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                highLightCurrentTab(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    private void highLightCurrentTab(int position) {
        for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = binding.tabLayout.getTabAt(i);
            assert tab != null;
            tab.setCustomView(null);
            tab.setCustomView(mainFragmentAdapter.getTabView(i));
        }
        TabLayout.Tab tab = binding.tabLayout.getTabAt(position);
        assert tab != null;
        tab.setCustomView(null);
        tab.setCustomView(mainFragmentAdapter.getSelectedTabView(position));
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}
