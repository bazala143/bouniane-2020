package com.bouniane.ui.customUi;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

public class CTextViewRegular extends AppCompatTextView {
    public CTextViewRegular(@NonNull Context context) {
        super(context);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Regular.ttf");
        this.setTypeface(typeface);
    }

    public CTextViewRegular(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Regular.ttf");
        this.setTypeface(typeface);
    }

    public CTextViewRegular(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Regular.ttf");
        this.setTypeface(typeface);
    }
}
