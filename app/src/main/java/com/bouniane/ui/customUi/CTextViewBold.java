package com.bouniane.ui.customUi;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class CTextViewBold extends androidx.appcompat.widget.AppCompatTextView {
    public CTextViewBold(@NonNull Context context) {
        super(context);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Bold.ttf");
        this.setTypeface(typeface);
        this.setTypeface(Typeface.DEFAULT_BOLD);
    }

    public CTextViewBold(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Bold.ttf");
        this.setTypeface(typeface);
        this.setTypeface(Typeface.DEFAULT_BOLD);
    }

    public CTextViewBold(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Bold.ttf");
        this.setTypeface(typeface);
        this.setTypeface(Typeface.DEFAULT_BOLD);
    }
}
