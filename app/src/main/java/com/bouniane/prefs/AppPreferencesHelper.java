/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.bouniane.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.bouniane.di.ApplicationContext;
import com.bouniane.di.PerActivity;
import com.bouniane.di.PreferenceInfo;
import com.google.gson.Gson;


import java.lang.reflect.Type;

import javax.inject.Inject;


/**
 * Created by Paras Andani on 27/01/17.
 */

@PerActivity
public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_DEVICE_TOKEN = "PREF_KEY_DEVICE_TOKEN";
    private static final String PREF_KEY_LOGIN_DETAILS = "PREF_KEY_LOGIN_DETAILS";
    private static final String PREF_KEY_IS_STARTED = "PREF_KEY_IS_STARTED";
    private static final String PREF_KEY_PROGRAM_ID = "PREF_KEY_PROGRAM_ID";
    private static final String PREF_KEY_LATITUDE = "PREF_KEY_LATITUDE";
    private static final String PREF_KEY_LONGITUDE = "PREF_KEY_LONGITUDE";
    private static final String PREF_KEY_LANGUAGE_CODE = "PREF_KEY_LANGUAGE_CODE";
    private static final String PREF_KEY_AUTHORIZATION_TOKEN = "PREF_KEY_AUTHORIZATION_TOKEN";
    private static final String PREF_KEY_IS_ACTIVITY_RUNNING = "PREF_KEY_IS_ACTIVITY_RUNNING";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(@ApplicationContext Context context,
                                @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public String getDeviceToken() {
        return mPrefs.getString(PREF_KEY_DEVICE_TOKEN, "111");
    }

    @Override
    public void setDeviceToken(String deviceToken) {
        mPrefs.edit().putString(PREF_KEY_DEVICE_TOKEN, deviceToken).apply();
    }


  /*  @Override
    public LoginDetails getLoginModel() {
        LoginDetails loginDetails;
        String response = getLoginDetails();
        if (!CommonUtils.isEmpty(response)) {
            final Gson gson = new Gson();
            Type collectionType = new TypeToken<LoginDetails>() {
            }.getType();
            loginDetails = gson.fromJson(response, collectionType);
            return loginDetails;
        }
        return null;
    }*/

    @Override
    public void setLoginDetails(String loginDetails) {
        mPrefs.edit().putString(PREF_KEY_LOGIN_DETAILS, loginDetails).apply();
    }

    @Override
    public boolean getIsStarted() {
        return mPrefs.getBoolean(PREF_KEY_IS_STARTED, false);
    }

    @Override
    public void setIsStarted(boolean isStarted) {
        mPrefs.edit().putBoolean(PREF_KEY_IS_STARTED, isStarted).apply();
    }

    @Override
    public String getProgramId() {
        return mPrefs.getString(PREF_KEY_PROGRAM_ID, "0");
    }

    @Override
    public void setProgramId(String programId) {
        mPrefs.edit().putString(PREF_KEY_PROGRAM_ID, programId).apply();
    }

    @Override
    public String getCurrentLatitude() {
        return mPrefs.getString(PREF_KEY_LATITUDE, "0");
    }

    @Override
    public void setCurrentLatitude(String latitude) {
        mPrefs.edit().putString(PREF_KEY_LATITUDE, latitude).apply();
    }

    @Override
    public String getCurrentLongitude() {
        return mPrefs.getString(PREF_KEY_LONGITUDE, "0");
    }

    @Override
    public void setCurrentLongitude(String longitude) {
        mPrefs.edit().putString(PREF_KEY_LONGITUDE, longitude).apply();
    }

    @Override
    public String getLanguageCode() {
        return mPrefs.getString(PREF_KEY_LANGUAGE_CODE, "en");
    }

    @Override
    public void setLanguageCode(String languageCode) {
        mPrefs.edit().putString(PREF_KEY_LANGUAGE_CODE, languageCode).apply();
    }

    @Override
    public String getAuthToken() {
        return mPrefs.getString(PREF_KEY_AUTHORIZATION_TOKEN, "");
    }

    @Override
    public void setAuthToken(String authToken) {
        mPrefs.edit().putString(PREF_KEY_AUTHORIZATION_TOKEN, authToken).apply();
    }

    private String getLoginDetails() {
        return mPrefs.getString(PREF_KEY_LOGIN_DETAILS, null);
    }


    @Override
    public boolean getIsFirstTimeLaunch() {
        return mPrefs.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    @Override
    public void setIsFirstTimeLaunch(boolean isFirstTimeLaunch) {
        mPrefs.edit().putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTimeLaunch).apply();
    }

    @Override
    public void setActivityRunning(boolean b) {
        mPrefs.edit().putBoolean(PREF_KEY_IS_ACTIVITY_RUNNING, b).apply();

    }

    @Override
    public boolean getIsAcitivtyRunning() {
        return mPrefs.getBoolean(PREF_KEY_IS_ACTIVITY_RUNNING, false);
    }


}
