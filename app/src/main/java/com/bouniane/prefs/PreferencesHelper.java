package com.bouniane.prefs;

public interface PreferencesHelper {

    String getDeviceToken();
    void setDeviceToken(String deviceToken);

   // LoginDetails getLoginModel();
    void setLoginDetails(String loginDetails);

    boolean getIsStarted();
    void setIsStarted(boolean isStarted);

    String getProgramId();
    void setProgramId(String programId);

    String getCurrentLatitude();
    void setCurrentLatitude(String latitude);

    String getCurrentLongitude();
    void setCurrentLongitude(String longitude);

    String getLanguageCode();
    void setLanguageCode(String languageCode);

    String getAuthToken();
    void setAuthToken(String authToken);

    boolean getIsFirstTimeLaunch();
    void setIsFirstTimeLaunch(boolean isFirstTimeLaunch);

    void setActivityRunning(boolean b);
    boolean getIsAcitivtyRunning();
}
